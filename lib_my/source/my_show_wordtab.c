/*
** my_show_wordtab.c for my_show_wordtab.c in /home/gouy_e/rendu/lib_my/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Jan 21 14:39:48 2014 gouy_e
** Last update Fri Apr 11 16:22:35 2014 gouy_e
*/

#include <stdlib.h>
#include "my.h"

int		my_show_wordtab(char **tab)
{
  int		i;

  i = -1;
  while (tab[++i] != NULL)
    {
      my_putstr(tab[i]);
      my_putchar('\n');
    }
  return (0);
}
