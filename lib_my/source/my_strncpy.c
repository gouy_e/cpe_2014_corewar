/*
** my_strncpy.c for my_strncpy.c in /home/gouy_e/rendu/lib_my/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Jan 21 15:06:34 2014 gouy_e
** Last update Fri Apr 11 16:27:51 2014 gouy_e
*/

char		*my_strncpy(char *dest, char *src, int n)
{
  int		i;

  i = -1;
  while (src[++i] && i < n)
    dest[i] = src[i];
  dest[i] = 0;
  return (dest);
}
