/*
** my_strlowcase.c for my_strlowcase.c in /home/gouy_e/rendu/lib_my/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Jan 21 15:01:17 2014 gouy_e
** Last update Fri Apr 11 16:26:50 2014 gouy_e
*/

char		*my_strlowcase(char *str)
{
  int		i;

  i = -1;
  while (str[++i] != 0)
    {
      if ((str[i] >= 65) && (str[i] <= 90))
	str[i] = str[i] + 32;
    }
  return (str);
}
