/*
** my_printf.c for my_printf.c in /home/Elliot/rendu/PSU_2013_my_printf/source
** 
** Made by Gouy Elliot
** Login   <Elliot@epitech.net>
** 
** Started on  Mon Nov 11 21:38:56 2013 Gouy Elliot
** Last update Fri Apr 11 16:30:41 2014 gouy_e
*/

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include "printf.h"
#include "my.h"

int		my_find_flag(t_flag tab[12], char *str, va_list aq, int *j)
{
  int		i;

  i = 0;
  if (str[*j + 1] == 0)
    {
      my_putchar(str[*j]);
      return (0);
    }
  if (str[*j + 1] == tab[12].data[0] && str[*j + 2] == tab[12].data[1])
    {
      *j = *j + 2;
      return (tab[12].fptr(aq));
    }
  while (tab[i].data != NULL)
    {
      if (str[*j + 1] == tab[i].data[0])
	{
	  *j = *j + 1;
	  return (tab[i].fptr(aq));
	}
      i = i + 1;
    }
  my_putchar(str[*j]);
  return (0);
}

void		my_get_tab(t_flag tab[13])
{
  tab[0].data = "s";
  tab[0].fptr = &my_putstr_arg;
  tab[1].data = "d";
  tab[1].fptr = &my_put_nbr_arg;
  tab[2].data = "p";
  tab[2].fptr = &my_get_nbr_base_addr;
  tab[3].data = "x";
  tab[3].fptr = &my_get_nbr_base_hex;
  tab[4].data = "X";
  tab[4].fptr = &my_get_nbr_base_HEX;
  tab[5].data = "o";
  tab[5].fptr = &my_get_nbr_base_oct;
  tab[6].data = "b";
  tab[6].fptr = &my_get_nbr_base_bin;
  tab[7].data = "S";
  tab[7].fptr = &my_put_printablestr;
  tab[8].data = "i";
  tab[8].fptr = &my_put_nbr_arg;
  tab[9].data = "c";
  tab[9].fptr = &my_putchar_arg;
  tab[10].data = "u";
  tab[10].fptr = &my_put_nbr_un;
  tab[11].data = NULL;
  tab[12].data = "ld";
  tab[12].fptr = &my_put_nbr_long;
}

int		my_printf(char *str, ...)
{
  t_flag	tab[13];
  va_list	aq;
  int		i;
  int		count;

  my_get_tab(&tab[0]);
  i = -1;
  count = 0;
  va_start(aq, str);
  while (str[++i] != 0)
    {
      if (str[i] == '%')
	count += my_find_flag(tab, str, aq, &i);
      else
	{
	  ++count;
	  my_putchar(str[i]);
	}
    }
  va_end(aq);
  return (count);
}
