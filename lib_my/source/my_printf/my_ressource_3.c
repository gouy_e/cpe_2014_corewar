/*
** my_ressource_3.c for my_ressource_3.c in /home/Elliot/rendu/PSU_2013_my_printf/source
** 
** Made by Gouy Elliot
** Login   <Elliot@epitech.net>
** 
** Started on  Wed Nov 13 15:57:54 2013 Gouy Elliot
** Last update Fri Apr 11 16:38:01 2014 gouy_e
*/

#include "my.h"

void		my_put_nbr_un_2(unsigned int nb)
{
  if (nb >= 10)
    {
      my_put_nbr_un_2(nb / 10);
    }
  my_putchar(nb % 10 + '0');
}

int		my_put_nbr_un(va_list aq)
{
  unsigned int	nbr;
  int		i;

  i = 0;
  nbr = va_arg(aq, unsigned int);
  my_put_nbr_un_2(nbr);
  while (nbr >= 1)
    {
      nbr = nbr / 10;
      i = i + 1;
    }
  return (i);
}

int		my_putchar_arg(va_list aq)
{
  char		c;

  c = va_arg(aq, int);
  my_putchar(c);
  return (1);
}

int		my_putstr_arg(va_list aq)
{
  char		*str;

  str = va_arg(aq, char *);
  my_putstr(str);
  return (my_strlen(str));
}

int		my_put_nbr_arg(va_list aq)
{
  int		nbr;

  nbr = va_arg(aq, int);
  my_put_nbr(nbr);
  return (my_intlen(nbr));
}
