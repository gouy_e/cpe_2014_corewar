/*
** my_strcat.c for my_strcat.c in /home/gouy_e/rendu/lib_my/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Jan 21 14:42:15 2014 gouy_e
** Last update Fri Apr 11 16:22:59 2014 gouy_e
*/

#include "my.h"

char		*my_strcat(char *dest, char *src)
{
  int		i;
  int		j;

  j = -1;
  i = my_strlen(dest);
  while (src[++j] != 0)
    {
      dest[i] = src[j];
      ++i;
    }
  dest[i] = '\0';
  return (dest);
}
