/*
** my_getnbr.c for my_getnbr.c in /home/gouy_e/rendu/lib_my/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Jan 21 14:46:38 2014 gouy_e
** Last update Fri Apr 11 16:19:10 2014 gouy_e
*/

int		isneg(char *str)
{
  int		i;
  int		j;

  j = -1;
  i = 0;
  while (str[++j] == 43 || str[j] == 45)
    {
      if (str[j] == 45)
	++i;
    }
  if (i % 2 == 0)
    return (0);
  else
    return (1);
}

int		nbr_is_int(long nb_2, int nbr)
{
  if (nbr != nb_2)
    return (0);
  return (nbr);
}

int		my_getnbr(char *str)
{
  int		nbr;
  int		i;
  long		nb_2;

  i = -1;
  nb_2 = 0;
  nbr = 0;
  while (((str[++i] >= 48) && (str[i] <= 57)) ||
	 (str[i] == 45 || str [i] == 43))
    {
      if ((str[i] <= 57) && (str[i] >= 48))
	{
	  nbr = (nbr * 10) + (str[i] - 48);
	  nb_2 = (nb_2 * 10) + (str[i] -48);
	}
    }
  nbr = nbr_is_int(nb_2, nbr);
  if (isneg(str) == 1)
    nbr = nbr * (-1);
  return (nbr);
}

int		my_getnbr_over(char *str)
{
  int		nbr;
  int		i;

  i = -1;
  nbr = 0;
  while (((str[++i] >= 48) && (str[i] <= 57)) ||
         (str[i] == 45 || str [i] == 43))
    {
      if ((str[i] <= 57) && (str[i] >= 48))
	nbr = (nbr * 10) + (str[i] - 48);
    }
  if (isneg(str) == 1)
    nbr = nbr * (-1);
  return (nbr);
}
