/*
** my_strncat.c for my_strncat.c in /home/gouy_e/rendu/lib_my/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Jan 21 15:02:18 2014 gouy_e
** Last update Fri Apr 11 16:27:15 2014 gouy_e
*/

char		*my_strncat(char *dest, char *src, int nb)
{
  int		i;
  int		j;

  j = -1;
  i = -1;
  while (dest[++i]);
  while (--nb != -1)
    {
      dest[i] = src[++j];
      ++i;
    }
  return (dest);
}
