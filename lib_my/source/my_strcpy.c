/*
** my_strcpy.c for my_strcpy.c in /home/gouy_e/rendu/lib_my/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Jan 21 14:43:36 2014 gouy_e
** Last update Fri Apr 11 16:23:34 2014 gouy_e
*/

char		*my_strcpy(char *dest, char *src)
{
  int		i;

  i = -1;
  while (src[++i] != 0)
    dest[i] = src[i];
  dest[i] = '\0';
  return (dest);
}
