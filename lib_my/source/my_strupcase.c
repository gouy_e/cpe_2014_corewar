/*
** my_strupcase.c for my_strupcase.c in /home/gouy_e/rendu/lib_my/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Jan 21 15:09:35 2014 gouy_e
** Last update Fri Apr 11 16:29:23 2014 gouy_e
*/

char		*my_strupcase(char *str)
{
  int		i;

  i = -1;
  while (str[++i] != 0)
    {
      if ((str[i] >= 97) && (str[i] <= 122))
	str[i] = str[i] - 32;
    }
  return (str);
}
