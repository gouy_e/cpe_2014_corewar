/*
** my_strcmp.c for my_strcmp.c in /home/Elliot/rendu/Piscine-C-Jour_06/ex_05
** 
** Made by Gouy Elliot
** Login   <Elliot@epitech.net>
** 
** Started on  Mon Oct  7 18:48:22 2013 Gouy Elliot
** Last update Fri Apr 11 16:23:17 2014 gouy_e
*/

#include "my.h"

int		my_strcmp(char *s1, char *s2)
{
  int		i;

  i = -1;
  while (s1[++i] && s2[i])
    {
      if (s1[i] < s2[i])
        return (-1);
      if (s1[i] > s2[i])
        return (1);
    }
  if (s1[i] < s2[i])
    return (-1);
  if (s1[i] > s2[i])
    return (1);
  return (0);
}
