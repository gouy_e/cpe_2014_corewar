/*
** my_putstr.c for my_putstr.c in /home/gouy_e/rendu/lib_my/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Jan 21 14:52:16 2014 gouy_e
** Last update Fri Apr 11 16:20:24 2014 gouy_e
*/

#include <unistd.h>
#include "my.h"

int		my_putstr(char *str)
{
  write(1, str, my_strlen(str));
  return (0);
}
