/*
** my_strncmp.c for my_strncmp.c in /home/gouy_e/rendu/lib_my/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Jan 21 15:02:48 2014 gouy_e
** Last update Sat Apr 12 14:24:42 2014 gouy_e
*/

#include "my.h"

int		my_strncmp(char *s1, char *s2, int n)
{
  int		i;

  i = 0;
  while (((s1[i] != 0) || (s2[i] != 0)) && (--n >= 0))
    {
      if (s1[i] > s2[i])
	return (1);
      else if (s1[i] < s2[i])
	return (-1);
      ++i;
    }
  return (0);
}
