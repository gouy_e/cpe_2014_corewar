/*
** my_puterror.c for my_puterror.c in /home/gouy_e/rendu/Minishell1.1/lib_my/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Wed Jan 22 11:41:25 2014 gouy_e
** Last update Mon Jan 27 18:47:59 2014 gouy_e
*/

#include <unistd.h>
#include "my.h"

void		my_puterror(char *str)
{
  write(2, str, my_strlen(str));
}
