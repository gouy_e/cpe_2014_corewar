/*
** my_strstr.c for my_strstr.c in /home/gouy_e/rendu/lib_my/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Jan 21 15:07:03 2014 gouy_e
** Last update Fri Apr 11 16:28:11 2014 gouy_e
*/

#include <stdlib.h>

char		*my_strstr(char *str, char *to_find)
{
  int		i;
  int		j;

  i = -1;
  j = 0;
  while (str[++i])
    {
      if (str[i] == to_find[j])
	++j;
      else
	j = 0;
      if (!to_find[j])
        {
          i = i - (j - 1);
          return (str + i);
        }
    }
  return (NULL);
}
