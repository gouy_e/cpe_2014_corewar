/*
** my_epurstr.c for my_epurstr.c in /home/gouy_e/rendu/CPE_2014_corewar/lib_my/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Feb 18 14:30:36 2014 gouy_e
** Last update Sat Feb 22 16:19:10 2014 gouy_e
*/

void		my_epurstr(char *str)
{
  int		i;
  int		j;

  i = -1;
  j = -1;
  while (str[++i])
    {
      if ((str[i] != ' ' && str[i] != '\t') ||
          ((str[i + 1] != ' ' && str[i + 1] != '\t') &&
           str[i + 1]))
        {
          if (str[i] == '\t')
            str[++j] = ' ';
          else
            str[++j] = str[i];
        }
    }
  str[++j] = 0;
  i = -1;
  j = -1;
  while (str[++i] && (str[i] == ' ' || str[i] == '\t'));
  --i;
  while (str[++i])
    str[++j] = str[i];
  str[++j] = str[i];
}
