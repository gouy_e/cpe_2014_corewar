/*
** my.h for gouy_e in /home/gouy_e/rendu/PSU_2013_my_ls/lib/headers
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Fri Nov 29 19:29:39 2013 gouy_e
** Last update Fri Apr 11 16:13:24 2014 gouy_e
*/

#ifndef MY_H_
# define MY_H_

# include <stdarg.h>

void	my_putchar(char);
void	my_putchar_error(char);
void	my_puterror(char *);
void	my_epurstr(char *);
int	my_put_nbr(int);
int	my_put_nbr_error(int);
int	my_putstr(char *);
int	my_strlen(char *);
int	my_getnbr(char *);
int	my_getnbr_over(char *);
int	my_strcmp(char *, char *);
int	my_strncmp(char *, char *, int);
char	*my_strcpy(char *, char *);
char	*my_strncpy(char *, char *, int);
char	*my_strlcpy(char *, char *, int);
char	*my_strcat(char *, char *);
char	*my_strncat(char *, char *, int );
char	*my_strstr(char *, char *);
char	*my_strupcase(char *);
char	*my_strlowcase(char *);
char	*my_revstr(char *);
char	**my_str_to_wordtab(char *);
int	my_printf(char *, ...);
int	my_putchar_arg(va_list);
int	my_putstr_arg(va_list);
int	my_put_nbr_arg(va_list);
int	my_put_nbr_long(va_list);
int	my_get_nbr_base_bin(va_list);
int	my_get_nbr_base_hex(va_list);
int	my_get_nbr_base_addr(va_list);
int	my_put_nbr_un(va_list);
int	my_get_nbr_base_HEX(va_list);
int	my_get_nbr_base_oct(va_list);
int	my_put_printablestr(va_list);
int	my_intlen(int);

#endif /* !MY_H_ */
