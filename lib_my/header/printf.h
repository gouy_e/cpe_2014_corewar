/*
** printf.h for corewar in /home/gouy_e/rendu/CPE_2014_corewar/lib_my/header
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Sun Apr 13 18:09:18 2014 gouy_e
** Last update Sun Apr 13 18:09:19 2014 gouy_e
*/

#ifndef PRINTF_H_
# define PRINTF_H_

# include <stdarg.h>

struct			s_flag
{
  char			*data;
  int			(*fptr)(va_list);
};

typedef struct s_flag	t_flag;

#endif /* !PRINTF_H_ */
