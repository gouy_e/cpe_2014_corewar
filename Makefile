##
## Makefile for corewar in /home/gouy_e/rendu/CPE_2014_corewar
## 
## Made by gouy_e
## Login   <gouy_e@epitech.net>
## 
## Started on  Thu Apr 10 19:59:36 2014 gouy_e
## Last update Sat Apr 26 14:30:48 2014 gouy_e
##

NAME_ASM	= asm/asm

NAME_VM		= corewar/corewar

NAME_BONUS	= bonus/graph

NAME_LIB	= lib_my/libmy.a

SRC_ASM		= asm/source/main.c \
		  asm/source/add_check.c \
		  asm/source/gtmd_check.c \
		  asm/source/aff_check.c \
		  asm/source/and_check.c \
		  asm/source/and_xor_or_check.c \
		  asm/source/check_asm_line.c \
		  asm/source/endian.c \
		  asm/source/fork_check.c \
		  asm/source/get_next_line.c \
		  asm/source/ld_check.c \
		  asm/source/ldi_check.c \
		  asm/source/lfork_check.c \
		  asm/source/live_check.c \
		  asm/source/lld_check.c \
		  asm/source/lldi_check.c \
		  asm/source/my_convert_arg2.c \
		  asm/source/my_convert_arg.c \
		  asm/source/my_convert_base.c \
		  asm/source/my_free.c \
		  asm/source/my_list_needed.c \
		  asm/source/my_msg_error.c \
		  asm/source/op.c \
		  asm/source/open_champ.c \
		  asm/source/or_check.c \
		  asm/source/parce.c \
		  asm/source/parce_label2.c \
		  asm/source/parce_label.c \
		  asm/source/parce_param.c \
		  asm/source/st_check.c \
		  asm/source/sti_check.c \
		  asm/source/sub_check.c \
		  asm/source/xor_check.c \
		  asm/source/zjmp_check.c

SRC_VM		= corewar/source/main.c \
		  corewar/source/file_create.c \
		  corewar/source/my_add.c \
		  corewar/source/my_aff.c \
		  corewar/source/my_and.c \
		  corewar/source/my_cp.c \
		  corewar/source/my_die.c \
		  corewar/source/my_fight.c \
		  corewar/source/my_fork.c \
		  corewar/source/my_get_args.c \
		  corewar/source/my_get_silent.c \
		  corewar/source/my_getfunc_args_2.c \
		  corewar/source/my_getfunc_args.c \
		  corewar/source/my_init.c \
		  corewar/source/my_init_func.c \
		  corewar/source/my_ld.c \
		  corewar/source/my_gtmd.c \
		  corewar/source/my_ldi.c \
		  corewar/source/my_live.c \
		  corewar/source/my_or.c \
		  corewar/source/my_put_in_end_list.c \
		  corewar/source/my_st.c \
		  corewar/source/my_sti.c \
		  corewar/source/my_sub.c \
		  corewar/source/my_take_option.c \
		  corewar/source/my_xor.c \
		  corewar/source/opts.c \
		  corewar/source/my_zjmp.c

SRC_BONUS	= bonus/source/main.c \
		  bonus/source/my_put_in_list.c \
		  bonus/source/cubes.c \
		  bonus/source/intro.c \
		  bonus/source/init.c \
		  bonus/source/scene.c \
		  bonus/source/text.c

SRC_LIB		= lib_my/source/my_strlen.c \
		  lib_my/source/my_getnbr.c \
		  lib_my/source/my_putchar.c \
		  lib_my/source/my_put_nbr.c \
		  lib_my/source/my_putstr.c \
		  lib_my/source/my_puterror.c \
		  lib_my/source/my_show_wordtab.c \
		  lib_my/source/my_strcat.c \
		  lib_my/source/my_strcmp.c \
		  lib_my/source/my_strcpy.c \
		  lib_my/source/my_strlcpy.c \
		  lib_my/source/my_strlowcase.c \
		  lib_my/source/my_strncat.c \
		  lib_my/source/my_strncmp.c \
		  lib_my/source/my_strncpy.c \
		  lib_my/source/my_strstr.c \
		  lib_my/source/my_revstr.c \
		  lib_my/source/my_epurstr.c \
		  lib_my/source/my_str_to_wordtab.c \
		  lib_my/source/my_printf/my_printf.c \
		  lib_my/source/my_printf/my_ressource.c \
		  lib_my/source/my_printf/my_ressource_2.c \
		  lib_my/source/my_printf/my_ressource_3.c

OBJ_ASM		= $(SRC_ASM:.c=.o)

OBJ_VM		= $(SRC_VM:.c=.o)

OBJ_BONUS	= $(SRC_BONUS:.c=.o)

OBJ_LIB		= $(SRC_LIB:.c=.o)

CC		= gcc

RM		= rm -f

SHELL		= /bin/bash

CFLAGS		= -W -Wextra -Werror -Wall -ansi -pedantic -O3

CFLAGS		+= -I lib_my/header/

CFLAGS		+= -I asm/header/

CFLAGS		+= -I corewar/header/

CFLAGS		+= -I bonus/header/

LDFLAGS		= -lmy -Llib_my

LDFLAGS2	= -lSDL -lGL -lGLU -lm -lfmodex -lglut

all:  $(NAME_LIB) $(NAME_ASM) $(NAME_VM) $(NAME_BONUS)

$(NAME_LIB): $(OBJ_LIB)
	ar rc $(NAME_LIB) $(OBJ_LIB) 
	ranlib $(NAME_LIB)

$(NAME_BONUS): $(OBJ_BONUS)
	$(CC) $(OBJ_BONUS) -o $(NAME_BONUS) $(LDFLAGS2) $(CFLAGS)

$(NAME_ASM): $(OBJ_ASM)
	$(CC) $(OBJ_ASM) -o $(NAME_ASM) $(LDFLAGS) $(CFLAGS)

$(NAME_VM): $(OBJ_VM)
	$(CC) $(OBJ_VM) -o $(NAME_VM) $(LDFLAGS) $(CFLAGS)

clean:
	$(RM) $(OBJ_LIB)
	$(RM) $(OBJ_ASM)
	$(RM) $(OBJ_VM)
	$(RM) $(OBJ_BONUS)

fclean: clean
	$(RM) $(NAME_LIB)
	$(RM) $(NAME_ASM)
	$(RM) $(NAME_VM)
	$(RM) $(NAME_BONUS)

re: fclean all
