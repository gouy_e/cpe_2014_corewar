.name "buathi_q"
.comment "Hello my name is Quentin !"

start:		ld %4, r3
		ld %8, r6
		ld %150994944, r8
		ld %250, r4
		ld %0, r2
trap1:		sti r1, %:live_dup, %1
trap2:		fork %:fork1

put_trap:	sti r8, %:trap1, %0
		sti r8, %:trap2, %0
		sti r8, %:end, %0

put_live:	sti r1, %:live, %1

live:		live %1
		zjmp %:live

dup_fork1:	fork %:dup_fork2
dup_fork2:	fork %:dup_fork
dup_fork3:	fork %:dup_fork
		zjmp %:dup_fork

fork1:		ld %0, r2
		fork %:fork2
		add r2, r6, r2

fork2:		fork %:init_dup
		add r2, r3, r2

init_dup:	ldi r2, %:dup, r5
		ld %0, r16
dup_fork:	fork %:live_dup
		zjmp %:dup_fork1

dup:		zjmp %0
live_dup:	live %1
		sti r5, r4, r2
		zjmp %248

end:
