.name "42Killer"
.comment "Hey 42 ! I kill you !"

init:	sti r1, %:live, %1
	sti r1, %:init, %1
	and r2, %0, r2
	fork %:live

fork1:	fork %:fork2
	zjmp %:lfork
fork2:	fork %:fork1

lfork:	lfork %4287 # (4260)
	zjmp %:fork1

live:	live %1
	zjmp %:live
