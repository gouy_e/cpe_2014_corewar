/*
** animation.h for corewar in /home/darnat_a/Documents/corewar_final
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu Apr 10 16:45:44 2014
** Last update Sun Apr 13 23:24:02 2014 gouy_e
*/

#ifndef ANIMATION_H_
# define ANIMATION_H_

# include <fmodex/fmod.h>
# include <SDL/SDL.h>

typedef enum		e_type
  {
    MEMACC		= 0,
    DEATH,
    WIN
  }			t_type;

typedef struct		s_music
{
  FMOD_SYSTEM		*system;
  FMOD_SOUND		*musique;
  FMOD_CHANNEL		*canal;
  FMOD_RESULT		resultat;
  float			spectre[512];
}			t_music;

typedef struct		s_animate
{
  t_type		type;
  double		state;
  double		value;
  int			pos;
  struct s_animate	*next;
}			t_animate;

typedef struct		s_color
{
  unsigned char		r;
  unsigned char		g;
  unsigned char		b;
}			t_color;

typedef struct		s_gen
{
  int			fd;
  t_plateau		plat;
  t_animate		*anim;
  int			key[2];
  double		params[2];
  t_color		colors[8];
  t_cube		cube;
  t_music		music;
  int			readok;
  int			nb_cycle;
  SDL_Surface		*ecran;
}			t_gen;

#endif /* !ANIMATION_H_ */
