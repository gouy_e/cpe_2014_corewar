/*
** anim.h for corewar in /home/darnat_a/Documents/corewar_final
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Mon Apr 14 00:25:57 2014
** Last update Mon Apr 14 01:05:51 2014 
*/

#ifndef ANIM_H_
# define ANIM_H_

int	draw_cubes(t_gen *, char vm[], int);
int	anim_memacc(t_gen *, t_animate *, t_animate *);
int	draw_scene(t_gen *n, char vm[], int *);
void	my_set_fps(int *, int *);

#endif /* !ANIM_H_ */
