/*
** functions.h for corewar in /home/darnat_a/Documents/corewar_final
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Mon Apr 14 00:10:59 2014
** Last update Mon Apr 14 00:17:58 2014 
*/

#ifndef FUNCTIONS_H_
# define FUNCTIONS_H_

int	my_aff_error(char *);
int	init_opengl();
int	my_read_file(t_gen *, char vm[], char *);
int	calc_width_height(t_plateau *);
int	play_music(t_gen *);
int	manage_key(t_gen *, char vm[]);

#endif /* !FUNCTIONS_H_ */
