/*
** scene.h for corewar in /home/darnat_a/Documents/corewar_final
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Mon Apr 14 00:21:21 2014
** Last update Mon Apr 14 00:25:17 2014 
*/

#ifndef SCENE_H_
# define SCENE_H_

int	draw_scene(t_gen *, char vm[], int *);
int	draw_texte(char *);
int	draw_nb_cycle(int, int);
int	verify_key(SDLKey, int *, int, int *);

#endif /* !SCENE_H_ */
