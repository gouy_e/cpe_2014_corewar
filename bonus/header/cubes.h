/*
** cubes.h for corewar in /home/darnat_a/Documents/corewar_final
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Mon Apr 14 00:18:17 2014
** Last update Mon Apr 14 00:20:18 2014 
*/

#ifndef CUBES_H_
# define CUBES_H_

void	check_cube_anim(t_gen *, int, char vm[]);

#endif /* !CUBES_H_ */
