/*
** graph.h for corewar in /home/darnat_a/Documents/corewar_final
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Tue Apr  8 19:07:44 2014
** Last update Sun Apr 13 23:25:24 2014 gouy_e
*/

#ifndef GRAPH_H_
# define GRAPH_H_

typedef struct		s_plateau
{
  int			height;
  int			width;
}			t_plateau;

typedef struct		s_cube
{
  float			x;
  float			y;
  float			z;
  float			sizex;
  float			sizey;
  float			sizez;
  int			redcolor;
  int			greencolor;
  int			bluecolor;
}			t_cube;

typedef unsigned int	size_v;

#endif /* !GRAPH_H_ */
