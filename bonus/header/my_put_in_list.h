/*
** my_put_in_list.h for corewar in /home/darnat_a/Documents/corewar_final
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu Apr 10 20:33:36 2014
** Last update Thu Apr 10 20:35:54 2014 
*/

#ifndef MY_PUT_IN_LIST_H_
# define MY_PUT_IN_LIST_H_

int	my_put_in_list(t_animate **, t_type, int);

#endif /* !MY_PUT_IN_LIST_H_ */
