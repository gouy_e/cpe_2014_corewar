/*
** main.c for corewar in /home/darnat_a/Documents/corewar_final/sources
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Tue Apr  8 16:18:52 2014
** Last update Sun Apr 13 23:27:49 2014 gouy_e
*/

#define _BSD_SOURCE

#include <time.h>
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>
#include <math.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <fmodex/fmod.h>
#include "graph.h"
#include "animation.h"
#include "op.h"
#include "my_put_in_list.h"

void		draw_texte(char* txt)
{
  int		i;

  i = -1;
  while (txt[++i])
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, txt[i]);
}

void		draw_nb_cycle(int cycle, int x)
{
  char		c[2];

  c[0] = cycle % 10 + 48;
  c[1] = 0;
  if (cycle > 9)
    {
      draw_nb_cycle(cycle / 10, x + 10);
      draw_texte(c);
    }
  else
    draw_texte(c);
}

int		my_aff_error(char *str)
{
  int		i;

  i = -1;
  while (str[++i]);
  write(2, str, i);
  return (1);
}
