/*
** main.c for corewar in /home/darnat_a/Documents/corewar_final/sources
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Tue Apr  8 16:18:52 2014
** Last update Sat Apr 26 14:26:41 2014 gouy_e
*/

#define _BSD_SOURCE

#include <time.h>
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>
#include <math.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <fmodex/fmod.h>
#include "graph.h"
#include "animation.h"
#include "functions.h"
#include "op.h"
#include "my_put_in_list.h"

void	init_colors_champs(t_gen *g)
{
  g->colors[0].r = 20;
  g->colors[0].g = 20;
  g->colors[0].b = 20;
  g->colors[1].r = 255;
  g->colors[1].g = 0;
  g->colors[1].b = 0;
  g->colors[2].r = 0;
  g->colors[2].g = 255;
  g->colors[2].b = 0;
  g->colors[3].r = 0;
  g->colors[3].g = 0;
  g->colors[3].b = 255;
  g->colors[4].r = 255;
  g->colors[4].g = 255;
  g->colors[4].b = 0;
  g->colors[5].r = 0;
  g->colors[5].g = 255;
  g->colors[5].b = 255;
  g->colors[6].r = 169;
  g->colors[6].g = 17;
  g->colors[6].b = 1;
  g->colors[7].r = 253;
  g->colors[7].g = 241;
  g->colors[7].b = 184;
}

int	init_struct(t_gen *g)
{
  FMOD_System_Create(&(g->music.system));
  FMOD_System_Init(g->music.system, 1, FMOD_INIT_NORMAL, NULL);
  g->music.resultat = FMOD_System_CreateSound(g->music.system,
					      "bonus/musics/musique_defaut.mp3",
				     FMOD_SOFTWARE | FMOD_2D | FMOD_CREATESTREAM,
				     0, &(g->music.musique));
  if (g->music.resultat != FMOD_OK)
    return (my_aff_error("Impossible de lire le fichier mp3\n"));
  g->key[0] = 0;
  g->key[1] = 0;
  g->params[0] = 10.;
  g->params[1] = 200.;
  g->anim = NULL;
  g->cube.z = 0;
  g->cube.sizex = 1.2;
  g->cube.sizey = 1.2;
  g->cube.sizez = 2.;
  g->cube.redcolor = 0;
  g->cube.greencolor = 0;
  g->cube.bluecolor = 0;
  g->readok = 0;
  g->nb_cycle = 0;
  init_colors_champs(g);
  return (0);
}

void		close_fmod(t_gen *g)
{
  FMOD_Sound_Release(g->music.musique);
  FMOD_System_Close(g->music.system);
  FMOD_System_Release(g->music.system);
}

int		init_window(t_gen *g)
{
  if (SDL_Init(SDL_INIT_VIDEO) == -1)
    return (1);
  SDL_WM_SetCaption("Corewar de PGM", NULL);
  g->ecran = SDL_SetVideoMode(1920, 1080, 32, SDL_OPENGL);
  if (g->ecran == NULL)
    return (1);
  init_opengl();
  return (0);
}

int		main(int ac, char **av)
{
  char		vm[MEM_SIZE];
  t_gen		g;

  if (ac == 1)
    return (my_aff_error("Error : Indiquez un fichier en paramètre.\n"));
  if (init_struct(&g))
    return (1);
  if (my_read_file(&g, vm, av[1]))
    return (my_aff_error("Error : read failed.\n"));
  glutInit(&ac, av);
  calc_width_height(&(g.plat));
  if (init_window(&g))
    return (1);
  atexit(SDL_Quit);
  play_music(&g);
  if (manage_key(&g, vm))
    return (1);
  close(g.fd);
  close_fmod(&g);
  return (0);
}
