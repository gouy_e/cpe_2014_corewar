/*
** main.c for corewar in /home/darnat_a/Documents/corewar_final/sources
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Tue Apr  8 16:18:52 2014
** Last update Sun Apr 13 23:26:40 2014 gouy_e
*/

#define _BSD_SOURCE

#include <time.h>
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>
#include <math.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <fmodex/fmod.h>
#include "struct_comb.h"
#include "graph.h"
#include "animation.h"
#include "op.h"
#include "my_put_in_list.h"

void		calc_width_height(t_plateau *plat)
{
  int		i;

  i = sqrt(MEM_SIZE) - 1;
  while (fmod((((double)MEM_SIZE) / ++i), 1.) != 0.);
  plat->height = i;
  plat->width = MEM_SIZE / i;
}

int		init_opengl()
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(70, (double)1920/1080, 1, 1000);
  glEnable(GL_DEPTH_TEST);
  glClearColor(0., 0., 0., 0);
  return (0);
}

int		my_read_file(t_gen *g, char vm[], char *filename)
{
  char		nb;
  header_t	head;
  int		i;

  if ((g->fd = open(filename, O_RDONLY)) == -1)
    {
      write(2, "Erreur : Impossible d'ouvrir le fichier.\n", 41);
      return (1);
    }
  if (read(g->fd, &nb, 1) == -1)
    return (1);
  i = nb + 1;
  while (--i)
    if (read(g->fd, &head, sizeof(header_t)) == -1)
      return (1);
  if (read(g->fd, vm, MEM_SIZE) == -1)
    return (1);
  return (0);
}

void		play_music(t_gen *g)
{
  FMOD_System_PlaySound(g->music.system, FMOD_CHANNEL_FREE,
			g->music.musique, 0, NULL);
  FMOD_System_GetChannel(g->music.system, 0, &(g->music.canal));
}
