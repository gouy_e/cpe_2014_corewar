/*
** main.c for corewar in /home/darnat_a/Documents/corewar_final/sources
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Tue Apr  8 16:18:52 2014
** Last update Sun Apr 13 23:28:53 2014 gouy_e
*/

#define _BSD_SOURCE

#include <time.h>
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>
#include <math.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <fmodex/fmod.h>
#include "struct_comb.h"
#include "graph.h"
#include "animation.h"
#include "op.h"
#include "anim.h"
#include "my_put_in_list.h"

void		check_cube_anim(t_gen *g, int i, char vm[])
{
  t_animate	*tmp;
  t_animate	*prev;
  int		c;
  int		calc;

  tmp = g->anim;
  prev = tmp;
  c = 1;
  g->cube.sizez = 2.;
  while (c && tmp)
    {
      calc = (tmp->pos % MEM_SIZE + MEM_SIZE) % MEM_SIZE;
      if (tmp->type == MEMACC)
	{
	  if (calc == i || calc == (i - 1) || calc == (i - 2) || calc == (i - 3))
	    anim_memacc(g, tmp, prev);
	}
      else if (tmp->type == DEATH)
	if (vm[i] == tmp->pos)
	  vm[i] = 0;
      prev = tmp;
      if (tmp)
	tmp = tmp->next;
    }
}

void		final_match(t_gen *g, char vm[], int *continuer, int id)
{
  int		tempsactuel;
  int		tempsprecedent;
  int		i;

  i = -1;
  *continuer = 0;
  g->readok = 2;
  tempsprecedent = 0;
  while (++i < MEM_SIZE)
    if (vm[i] != id)
      vm[i] = 0;
  i = -1;
  while (++i < 100)
    {
      draw_scene(g, vm, continuer);
      my_set_fps(&tempsprecedent, &tempsactuel);
    }
  i = -1;
  while (++i < 150)
    {
      g->cube.z += i / 10;
      draw_scene(g, vm, continuer);
      my_set_fps(&tempsprecedent, &tempsactuel);
    }
}

int		my_modif_scene(char vm[], t_gen *g, int *continuer)
{
  t_comb	params;
  int		s;

  if (g->readok != 1)
    return (0);
  if ((s = read(g->fd, &params, sizeof(t_comb))) == -1)
    return (1);
  if (params.opt == 0)
    {
      my_put_in_list(&(g->anim), MEMACC, params.pos);
      g->nb_cycle = params.nb_cycle;
      params.pos = (params.pos % MEM_SIZE + MEM_SIZE) % MEM_SIZE;
      vm[params.pos] = params.id;
      vm[(params.pos + 1) % MEM_SIZE] = params.id;
      vm[(params.pos + 2) % MEM_SIZE] = params.id;
      vm[(params.pos + 3) % MEM_SIZE] = params.id;
    }
  else if (params.opt == 3)
    my_put_in_list(&(g->anim), DEATH, params.id);
  else if (params.opt == 4)
    final_match(g, vm, continuer, params.id);
  return (0);
}

int		get_height_spectre(t_gen *g)
{
  double	n;
  int		i;

  FMOD_Channel_GetSpectrum(g->music.canal, g->music.spectre,
			   512, 0,  FMOD_DSP_FFT_WINDOW_RECT);
  i = -1;
  n = 0.0;
  while (++i < 200)
    n += g->music.spectre[i];
  n /= i;
  n *= 300;
  if (n > 40)
    n = 0;
  return (n);
}

int		draw_scene(t_gen *g, char vm[], int *continuer)
{
  double	xcam;
  double	ycam;
  double	zcam;
  int		n;

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  xcam = 150 * cos(g->params[0]);
  ycam = 150 * sin(g->params[0]);
  zcam = 80 + g->params[1];
  gluLookAt(xcam, ycam, zcam, 0, 0, 0, 0, 0, 1);
  g->cube.x = g->plat.height;
  g->cube.y = g->plat.width;
  n = get_height_spectre(g);
  if (my_modif_scene(vm, g, continuer))
    return (1);
  glBegin(GL_QUADS);
  draw_cubes(g, vm, n);
  glEnd();
  glFlush();
  SDL_GL_SwapBuffers();
  return (0);
}
