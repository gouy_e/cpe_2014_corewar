/*
** main.c for corewar in /home/darnat_a/Documents/corewar_final/sources
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Tue Apr  8 16:18:52 2014
** Last update Sun Apr 13 23:28:36 2014 gouy_e
*/

#define _BSD_SOURCE

#include <time.h>
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>
#include <math.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <fmodex/fmod.h>
#include "graph.h"
#include "animation.h"
#include "cubes.h"
#include "op.h"
#include "my_put_in_list.h"

int		verify_key(SDLKey sym, int *key, int key_dir, int *continuer)
{
  if (sym == SDLK_UP)
    key[0] = (key_dir) ? 1 : 0;
  else if (sym == SDLK_DOWN)
    key[0] = (key_dir) ? -1 : 0;
  else if (sym == SDLK_RIGHT)
    key[1] = (key_dir) ? 1 : 0;
  else if (sym == SDLK_LEFT)
    key[1] = (key_dir) ? -1 : 0;
  else if (!key_dir && sym == SDLK_ESCAPE)
    *continuer = 0;
  return (0);
}

void		aff_cube_part2(t_cube cube)
{
  glColor3ub(cube.redcolor * 0.4, cube.greencolor * 0.4, cube.bluecolor * 0.4);
  glVertex3d(cube.x, cube.y + cube.sizey, cube.z);
  glVertex3d(cube.x + cube.sizex, cube.y + cube.sizey, cube.z);
  glVertex3d(cube.x + cube.sizex, cube.y + cube.sizey, cube.z + cube.sizez);
  glVertex3d(cube.x, cube.y + cube.sizey, cube.z + cube.sizez);
  glColor3ub(cube.redcolor, cube.greencolor, cube.bluecolor);
  glVertex3d(cube.x, cube.y, cube.z + cube.sizez);
  glVertex3d(cube.x + cube.sizex, cube.y, cube.z + cube.sizez);
  glVertex3d(cube.x + cube.sizex, cube.y + cube.sizey, cube.z + cube.sizez);
  glVertex3d(cube.x, cube.y + cube.sizey, cube.z + cube.sizez);
}

void		my_aff_cube(t_cube cube)
{
  glColor3ub(cube.redcolor * 0.4, cube.greencolor * 0.4, cube.bluecolor * 0.4);
  glVertex3d(cube.x, cube.y, cube.z);
  glVertex3d(cube.x + cube.sizex, cube.y, cube.z);
  glVertex3d(cube.x + cube.sizex, cube.y + cube.sizey, cube.z);
  glVertex3d(cube.x, cube.y + cube.sizey, cube.z);
  glVertex3d(cube.x, cube.y, cube.z);
  glVertex3d(cube.x + cube.sizex, cube.y, cube.z);
  glVertex3d(cube.x + cube.sizex, cube.y, cube.z + cube.sizez);
  glVertex3d(cube.x, cube.y, cube.z + cube.sizez);
  glColor3ub(cube.redcolor * 0.6, cube.greencolor * 0.6, cube.bluecolor * 0.6);
  glVertex3d(cube.x, cube.y, cube.z);
  glVertex3d(cube.x, cube.y + cube.sizey, cube.z);
  glVertex3d(cube.x, cube.y + cube.sizey, cube.z + cube.sizez);
  glVertex3d(cube.x, cube.y, cube.z + cube.sizez);
  glColor3ub(cube.redcolor * 0.6, cube.greencolor * 0.6, cube.bluecolor * 0.6);
  glVertex3d(cube.x + cube.sizex, cube.y, cube.z);
  glVertex3d(cube.x + cube.sizex, cube.y + cube.sizey, cube.z);
  glVertex3d(cube.x + cube.sizex, cube.y + cube.sizey, cube.z + cube.sizez);
  glVertex3d(cube.x + cube.sizex, cube.y, cube.z + cube.sizez);
  aff_cube_part2(cube);
}

int		anim_memacc(t_gen *g, t_animate *tmp, t_animate *prev)
{
  g->cube.sizez = tmp->value;
  tmp->value += tmp->state;
  if (tmp->value == 20)
    tmp->state = -0.5;
  if (tmp->value == 1)
    {
      tmp->state = 0;
      if (prev == tmp)
	g->anim = tmp->next;
      else
	prev->next = tmp->next;
      free(tmp);
    }
  return (0);
}

int		draw_cubes(t_gen *g, char vm[], int n)
{
  int		i;

  i = -1;
  while (++i < MEM_SIZE)
    {
      if ((i % g->plat.height) == 0)
	{
	  g->cube.x = g->plat.height;
	  g->cube.y -= 2.0;
	}
      check_cube_anim(g, i, vm);
      if (vm[i])
	g->cube.sizez += n / 2;
      else
	g->cube.sizez = n / 4. + 0.2;
      g->cube.redcolor = g->colors[vm[i] % 8].r;
      g->cube.greencolor = g->colors[vm[i] % 8].g;
      g->cube.bluecolor = g->colors[vm[i] % 8].b;
      my_aff_cube(g->cube);
      g->cube.x -= 2.0;
    }
  return (0);
}
