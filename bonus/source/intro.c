/*
** main.c for corewar in /home/darnat_a/Documents/corewar_final/sources
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Tue Apr  8 16:18:52 2014
** Last update Sun Apr 13 23:27:21 2014 gouy_e
*/

#define _BSD_SOURCE

#include <time.h>
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>
#include <math.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <fmodex/fmod.h>
#include "graph.h"
#include "animation.h"
#include "op.h"
#include "scene.h"
#include "my_put_in_list.h"

int		wait_for_intro(t_gen *g, char vm[], int *continuer)
{
  int		tempsactuel;
  int		tempsprecedent;
  double	subnb;

  subnb = -1;
  tempsprecedent = 0;
  while (++subnb < 94)
    {
      if (draw_scene(g, vm, continuer))
	return (1);
      tempsactuel = SDL_GetTicks();
      if (tempsactuel - tempsprecedent < 25)
	SDL_Delay(25 - (tempsactuel - tempsprecedent));
      tempsprecedent = SDL_GetTicks();
    }
  g->readok = 1;
  return (0);
}

int		start_intro(t_gen *g, char vm[], int *continuer)
{
  int		tempsactuel;
  int		tempsprecedent;
  double	subnb;

  tempsprecedent = 0;
  subnb = 1.;
  while (g->params[1] != 0)
    {
      if (g->params[1] == 10 || g->params[1] == 4)
	subnb /= 2;
      g->params[0] -= subnb / 20.;
      g->params[1] -= subnb;
      if (draw_scene(g, vm, continuer))
	return (1);
      tempsactuel = SDL_GetTicks();
      if (tempsactuel - tempsprecedent < 25)
	SDL_Delay(25 - (tempsactuel - tempsprecedent));
      tempsprecedent = SDL_GetTicks();
    }
  if (wait_for_intro(g, vm, continuer))
    return (1);
  return (0);
}

void		my_set_text(t_gen *g)
{
  glColor3f(255, 255, 255);
  glRasterPos3f(-96, -20, 80);
  draw_texte(" nombres de cycles : ");
  draw_nb_cycle(g->nb_cycle, 0);
}

void		my_set_fps(int *tempsprecedent, int *tempsactuel)
{
  *tempsactuel = SDL_GetTicks();
  if (*tempsactuel - *tempsprecedent < 25)
    SDL_Delay(25 - (*tempsactuel - *tempsprecedent));
  *tempsprecedent = SDL_GetTicks();
}

int		manage_key(t_gen *g, char vm[])
{
  SDL_Event	event;
  int		continuer;
  int		tempsactuel;
  int		tempsprecedent;

  continuer = 1;
  tempsprecedent = 0;
  start_intro(g, vm, &continuer);
  while (continuer)
    {
      SDL_PollEvent(&event);
      if (event.type == SDL_QUIT)
	continuer = 0;
      else if (event.type == SDL_KEYDOWN)
	verify_key(event.key.keysym.sym, g->key, 1, &continuer);
      else if (event.type == SDL_KEYUP)
	verify_key(event.key.keysym.sym, g->key, 0, &continuer);
      if (draw_scene(g, vm, &continuer))
	continuer = 0;
      g->params[0] += g->key[1] / 20.;
      g->params[1] += g->key[0] * 4;
      my_set_text(g);
      my_set_fps(&tempsprecedent, &tempsactuel);
    }
  return (0);
}
