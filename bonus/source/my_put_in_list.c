/*
** my_put_in_list.c for my_put_in_list in /home/darnat_a/Documents/corewar_final
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu Apr 10 20:16:24 2014
** Last update Sun Apr 13 23:13:00 2014 gouy_e
*/

#include <stdlib.h>
#include "graph.h"
#include "animation.h"

int		my_put_in_list(t_animate **fa, t_type type, int pos)
{
  t_animate	*elem;

  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (1);
  elem->type = type;
  elem->state = 0.5;
  elem->value = 2;
  elem->pos = pos;
  elem->next = *fa;
  *fa = elem;
  return (0);
}
