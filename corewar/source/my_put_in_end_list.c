/*
** my_put_in_end_list.c for corewar in /home/gouy_e/corewar/sources
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Sun Mar 30 22:29:01 2014 gouy_e
** Last update Sun Apr 13 21:09:05 2014 gouy_e
*/

#include <stdlib.h>
#include "op.h"
#include "champ.h"

void		my_set_newprocess(t_champ *elem, int opts[3])
{
  elem->next = NULL;
  elem->base = opts[2];
  elem->id = opts[0];
  (elem->reg)[1] = opts[0];
  elem->pc = opts[1];
  elem->cycle = 0;
  elem->carry = 0;
  elem->live = 0;
  elem->body = NULL;
}

int		my_put_in_end_list(t_champ **info,
				   int opts[3], int size[2])
{
  t_champ	*elem;
  t_champ	*tmp;

  elem = malloc(sizeof(*elem));
  if (elem == NULL)
    return (1);
  my_set_newprocess(elem, opts);
  elem->min = size[0];
  elem->max = size[1];
  if (*info == NULL)
    *info = elem;
  else
    {
      tmp = *info;
      while (tmp->next != NULL)
        tmp = tmp->next;
      tmp->next = elem;
    }
  return (0);
}
