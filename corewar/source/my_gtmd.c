/*
** my_gtmd.c for corewar in /home/gouy_e/rendu/CPE_2014_corewar/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Sun Apr 13 18:41:44 2014 gouy_e
** Last update Sun Apr 13 22:02:59 2014 gouy_e
*/

#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "read_oct.h"
#include "my.h"

int		my_gtmd(size_v vm[], t_champ *champ, t_vmprop *vmprop)
{
  get_func_arg(champ, vm);
  if ((champ->arg)[0] >= 1 && (champ->arg)[0] <= REG_NUMBER)
    {
      if (((champ->reg)[(champ->arg)[0]] = vmprop->ctmo))
        champ->carry = 0;
      else
        champ->carry = 1;
    }
  return (0);
}
