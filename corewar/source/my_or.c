/*
** my_or.c for corewar in /home/gouy_e/rendu/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Mon Apr  7 22:31:05 2014 gouy_e
** Last update Mon Apr  7 23:15:51 2014 gouy_e
*/

#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "read_oct.h"
#include "my.h"

int		my_or(size_v vm[], t_champ *champ)
{
  get_func_arg(champ, vm);
  if ((champ->arg)[2] >= 1 && (champ->arg)[2] <= REG_NUMBER)
    {
      if (((champ->reg)[(champ->arg)[2]] = (champ->val)[0] | (champ->val)[1]))
	champ->carry = 0;
      else
	champ->carry = 1;
    }
  return (0);
}
