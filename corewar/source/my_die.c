/*
** my_die.c for corewar in /home/gouy_e/rendu/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Apr  8 15:33:46 2014 gouy_e
** Last update Fri Apr 11 19:45:38 2014 gouy_e
*/

#include <stdlib.h>
#include "op.h"
#include "champ.h"
#include "vmprop.h"

void		my_rm_node_in_list(t_champ *list)
{
  t_champ	*tmp;

  tmp = list->next;
  list->next = list->next->next;
  free(tmp->body);
  free(tmp);
}

void		my_die(int id, t_vmprop *vmprop)
{
  t_champ	*tmp;

  tmp = vmprop->deb;
  while (tmp->next != NULL)
    {
      if (tmp->next->id == id)
	my_rm_node_in_list(tmp);
      else
	tmp = tmp->next;
    }
  tmp = vmprop->deb;
  if (tmp != NULL && tmp->id == id)
    {
      vmprop->deb = tmp->next;
      free(tmp->body);
      free(tmp);
    }
}
