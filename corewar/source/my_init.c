/*
** my_init.c for corewar in /home/gouy_e/rendu/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Thu Apr 10 18:28:00 2014 gouy_e
** Last update Sun Apr 13 22:18:17 2014 gouy_e
*/

#include <stdlib.h>
#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "struct_arg.h"
#include "get_arg.h"
#include "my.h"

int		my_init_tabarg(t_arg **tab)
{
  if ((*tab = malloc(sizeof(t_arg) * 6)) == NULL)
    return (1);
  (*tab)[0].pattern = "-a";
  (*tab)[0].fptr = my_get_a_arg;
  (*tab)[1].pattern = "-n";
  (*tab)[1].fptr = my_get_n_arg;
  (*tab)[2].pattern = "-dump";
  (*tab)[2].fptr = my_get_dump_arg;
  (*tab)[3].pattern = "-X";
  (*tab)[3].fptr = my_get_x_arg;
  (*tab)[4].pattern = "-silent";
  (*tab)[4].fptr = my_get_silent_arg;
  (*tab)[5].pattern = "-ctmo";
  (*tab)[5].fptr = my_get_ctmo_arg;
  return (0);
}

void		my_init_reg(t_champ *list)
{
  t_champ	*tmp;
  int		i;
  char		temp;

  tmp = list;
  temp = 0;
  while (tmp != NULL)
    {
      i = 1;
      tmp->id_x = (char)++temp;
      while (++i != REG_NUMBER + 1)
        (tmp->reg)[i] = 0;
      tmp = tmp->next;
    }
}

void		my_clean_vm(size_v vm[])
{
  int		i;

  i = -1;
  while (++i != MEM_SIZE)
    vm[i] = 0;
}

int		my_put_champ_in_vm(t_vmprop *vmprop, size_v vm[],
				   t_champ *tmp, int nb)
{
  int		pos;
  int		i;
  int		a;

  if (tmp->pc == -1)
    {
      pos = (nb * MEM_SIZE) / vmprop->champ_tot;
      tmp->pc = pos;
    }
  else
    pos = tmp->pc;
  tmp->min = pos;
  tmp->max = (pos + tmp->head.prog_size - 1) % MEM_SIZE;
  i = -1;
  a = -1;
  while (++i != tmp->head.prog_size)
    {
      pos = pos % MEM_SIZE;
      if (vm[pos] != 0 && ++a == 0)
	return (-1);
      vm[pos++] = (tmp->body)[i];
    }
  return (0);
}

int		my_init_vm(t_vmprop *vmprop, size_v vm[])
{
  t_champ	*tmp;
  int		nb;

  tmp = vmprop->deb;
  nb = -1;
  my_clean_vm(vm);
  while (tmp != NULL && ++nb != 8)
    {
      if (my_put_champ_in_vm(vmprop, vm, tmp, nb) == -1)
	{
	  my_puterror("Champions are overwritting in memory... Aborting\n");
	  return (-1);
	}
      tmp = tmp->next;
    }
  return (0);
}
