/*
** my_init_func.c for Corewar in /home/hirt_r/corewar
** 
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
** 
** Started on  Mon Mar 31 23:50:28 2014 Romain Hirt
** Last update Sun Apr 13 18:40:26 2014 gouy_e
*/

#include <stdlib.h>
#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "champ_func.h"

void		init_cycle(int tab[])
{
  tab[1] = 10;
  tab[2] = 5;
  tab[3] = 5;
  tab[4] = 10;
  tab[5] = 10;
  tab[6] = 6;
  tab[7] = 6;
  tab[8] = 6;
  tab[9] = 20;
  tab[10] = 25;
  tab[11] = 25;
  tab[12] = 800;
  tab[13] = 10;
  tab[14] = 50;
  tab[15] = 1000;
  tab[16] = 2;
  tab[17] = 10;
}

void		init_func(t_vmprop vmprop[])
{
  (vmprop->ptr)[1] = my_live;
  (vmprop->ptr)[2] = my_ld;
  (vmprop->ptr)[3] = my_st;
  (vmprop->ptr)[4] = my_add;
  (vmprop->ptr)[5] = my_sub;
  (vmprop->ptr)[6] = my_and;
  (vmprop->ptr)[7] = my_or;
  (vmprop->ptr)[8] = my_xor;
  (vmprop->ptr)[9] = my_zjmp;
  (vmprop->ptr)[10] = my_ldi;
  (vmprop->ptr)[11] = my_sti;
  (vmprop->ptr)[12] = my_fork;
  (vmprop->ptr)[13] = my_ld;
  (vmprop->ptr)[14] = my_ldi;
  (vmprop->ptr)[15] = my_fork;
  (vmprop->ptr)[16] = my_aff;
  (vmprop->ptr)[17] = my_gtmd;
}
