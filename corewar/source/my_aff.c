/*
** my_aff.c for corewar in /home/gouy_e/rendu/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Apr  8 01:59:15 2014 gouy_e
** Last update Sun Apr 13 21:27:13 2014 gouy_e
*/

#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "read_oct.h"
#include "my.h"

int		my_aff(size_v vm[], t_champ *champ)
{
  get_func_arg(champ, vm);
  my_putchar(((champ->val)[0] % 256));
  return (0);
}
