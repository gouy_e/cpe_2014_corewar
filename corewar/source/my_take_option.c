/*
** my_take_option.c for corewar in /home/gouy_e/rendu/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Thu Apr 10 17:07:24 2014 gouy_e
** Last update Sun Apr 13 22:20:46 2014 gouy_e
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "struct_arg.h"
#include "my.h"
#include "my_list_needed.h"
#include "get_arg.h"

int		get_free_id(t_champ *list)
{
  int		id;
  t_champ	*tmp;

  tmp = list;
  id = 1;
  while (tmp != NULL)
    {
      if (id == tmp->id)
        {
          tmp = list;
          ++id;
        }
      else
        tmp = tmp->next;
    }
  return (id);
}

int		my_read_in_champ(int fd, t_champ **list)
{
  t_champ	*tmp;

  tmp = *list;
  while (tmp != NULL && tmp->next != NULL)
    tmp = tmp->next;
  if (read(fd, &(tmp->head), sizeof(header_t)) != sizeof(header_t) ||
      reverse_endian((tmp->head).magic) != COREWAR_EXEC_MAGIC)
    return (-2);
  (tmp->head).prog_size = reverse_endian((tmp->head).prog_size);
  my_epurstr((tmp->head).prog_name);
  my_epurstr((tmp->head).comment);
  if ((tmp->body = malloc((tmp->head).prog_size)) == NULL)
    {
      my_puterror("can't perform malloc\n");
      return (-1);
    }
  if (read(fd, tmp->body, (tmp->head).prog_size) != (tmp->head).prog_size)
    return (-2);
  return (0);
}

int		my_set_champ_in_list(char *name, t_champ **list,
				     int opts[3], int fd)
{
  int		tmp;
  int		size[2];

  size[0] = 0;
  size[1] = 0;
  if (my_put_in_end_list(list, opts, size) == 1)
    {
      my_puterror("can't perform malloc\n");
      return (-1);
    }
  if ((tmp = my_read_in_champ(fd, list)) == -2)
    {
      my_puterror(name);
      my_puterror(": bad champion file\n");
      return (-1);
    }
  return (tmp);
}

int		my_get_champ_arg(char *name, t_champ **list, int opts[3])
{
  int		fd;
  int		tmp;

  if ((fd = open(name, O_RDONLY)) == -1)
    {
      my_puterror("can't perform open on ");
      my_puterror(name);
      my_puterror("\n");
      return (-1);
    }
  if (opts[0] == -1)
    opts[0] = get_free_id(*list);
  tmp = my_set_champ_in_list(name, list, opts, fd);
  close(fd);
  return (tmp);
}

int		my_set_option(char **av, t_vmprop *vmprop, t_arg *tab)
{
  int		k;
  int		i;
  int		opts[3];

  i = 0;
  opts[0] = -1;
  opts[1] = -1;
  opts[2] = 0;
  vmprop->fd = -1;
  while (av[++i] && vmprop->champ_tot != 9)
    {
      k = -1;
      while (++k != 6 && my_strcmp(tab[k].pattern, av[i]) != 0);
      if (k < 6 && av[i + 1])
        tab[k].fptr(av[++i], &opts, vmprop);
      else
        {
          if (my_get_champ_arg(av[i], &(vmprop->deb), opts) != 0)
            return (-1);
          ++(vmprop->champ_tot);
          opts[0] = -1;
          opts[1] = -1;
        }
    }
  return (i);
}
