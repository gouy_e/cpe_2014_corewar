/*
** my_zjmp.c for corewar in /home/gouy_e/rendu/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Mon Apr  7 22:54:10 2014 gouy_e
** Last update Mon Apr  7 23:35:30 2014 gouy_e
*/

#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "read_oct.h"
#include "my.h"

int		my_zjmp(size_v vm[], t_champ *champ)
{
  int		value;
  int		tmp;

  tmp = champ->pc;
  value = my_addr_ind(champ, vm, 9);
  if (champ->carry == 1)
    champ->pc = ((value + tmp) % MEM_SIZE + MEM_SIZE) % MEM_SIZE;
  else
    champ->pc = (champ->pc + 1) % MEM_SIZE;
  return (0);
}
