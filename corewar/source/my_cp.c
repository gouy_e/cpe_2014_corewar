/*
** my_cp.c for corewar in /home/gouy_e/rendu/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Mon Apr  7 22:06:18 2014 gouy_e
** Last update Mon Apr  7 23:32:02 2014 gouy_e
*/

#include "op.h"
#include "champ.h"
#include "vmprop.h"

void		my_cp(size_v vm[], int pc, int value)
{
  int		i;

  i = -1;
  pc = (pc % MEM_SIZE + MEM_SIZE) % MEM_SIZE;
  while (++i != 4)
    vm[(pc + i) % MEM_SIZE] = (value >> ((3 - i) * 8)) & 0xff;
}
