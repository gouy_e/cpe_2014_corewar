/*
** my_ld.c for corewar in /home/gouy_e/rendu/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Mon Apr  7 21:05:55 2014 gouy_e
** Last update Sun Apr 13 18:46:09 2014 gouy_e
*/

#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "read_oct.h"
#include "my.h"

int		my_ld(size_v vm[], t_champ *champ)
{
  get_func_arg(champ, vm);
  if ((champ->arg)[1] >= 1 && (champ->arg)[1] <= REG_NUMBER)
    {
      if (((champ->reg)[(champ->arg)[1]] = (champ->val)[0]))
	champ->carry = 0;
      else
	champ->carry = 1;
    }
  return (0);
}
