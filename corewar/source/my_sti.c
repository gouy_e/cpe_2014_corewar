/*
** my_sti.c for corewar in /home/gouy_e/rendu/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Mon Apr  7 23:26:41 2014 gouy_e
** Last update Sun Apr 13 21:09:24 2014 gouy_e
*/

#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "read_oct.h"
#include "my.h"
#include "file_create.h"

int		my_sti(size_v vm[], t_champ *champ, t_vmprop *vmprop)
{
  int		s;
  int		tmp;
  int		temp;

  tmp = champ->pc;
  get_func_arg(champ, vm);
  s = (champ->val)[1] + (champ->val)[2];
  temp = ((tmp + s) % MEM_SIZE + MEM_SIZE) % MEM_SIZE;
  if (vmprop->nb_cycle > vmprop->ctmo ||
      (temp >= champ->min && temp <= champ->max))
    {
      my_write(champ, vmprop, 0, s + tmp);
      my_cp(vm, tmp + s, (champ->val)[0]);
    }
  return (0);
}
