/*
** main.c for corewar in /home/gouy_e/corewar/sources
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Sun Mar 30 22:23:29 2014 gouy_e
** Last update Sun Apr 13 22:15:27 2014 gouy_e
*/

#include <stdlib.h>
#include <unistd.h>
#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "struct_arg.h"
#include "my.h"
#include "fight.h"
#include "file_create.h"
#include "init.h"
#include "get_arg.h"

void		my_free_list(t_vmprop *vmprop)
{
  t_champ	*tmp;

  if (vmprop->fd != -1)
    close(vmprop->fd);
  while (vmprop->deb != NULL)
    {
      tmp = vmprop->deb;
      vmprop->deb = vmprop->deb->next;
      free(tmp->body);
      free(tmp);
    }
}

void		my_aff_usage(char *name)
{
  my_puterror("Usage : ");
  my_puterror(name);
  my_puterror("\n\t[-dump nbr_cycle] [-X fight_file]\n");
  my_puterror("\t[[-n prog_number] [-a load_addres] champ_file] ...\n" );
}

int		start_option(char **av, size_v vm[], t_vmprop *vmprop)
{
  t_arg		*tab;
  int		tmp;

  my_init_tabarg(&tab);
  tmp = my_set_option(av, vmprop, tab);
  free(tab);
  if (tmp == -1)
    return (1);
  else if (tmp > 0 && av[tmp])
    my_puterror("Warning too much champions charged\n");
  if (my_init_vm(vmprop, vm) != 0)
    return (1);
  my_init_reg(vmprop->deb);
  if (vmprop->champ_tot <= 0)
    {
      my_puterror("No valid champion to run, exiting...\n");
      return (1);
    }
  my_init_write(vmprop, vm);
  return (0);
}

int		main(int ac, char **av)
{
  t_vmprop	vmprop;
  size_v	vm[MEM_SIZE];
  int		tmp;

  if (ac == 1)
    {
      my_aff_usage(av[0]);
      return (0);
    }
  vmprop.dump = -1;
  vmprop.champ_tot = 0;
  vmprop.deb = NULL;
  vmprop.silent = 0;
  vmprop.ctmo = 0;
  if (start_option(av, vm, &vmprop) != 0)
    {
      my_free_list(&vmprop);
      return (1);
    }
  if ((tmp = my_start_fight(vm, &vmprop)) == 0)
    {
      my_write(vmprop.last, &vmprop, 4, 0);
      my_printf("%s (%d) a gagné\n",
		vmprop.last->head.prog_name, vmprop.last->id);
    }
  my_free_list(&vmprop);
  return (0);
}
