/*
** opts.c for corewar in /home/gouy_e/rendu/CPE_2014_corewar/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Sun Apr 13 15:39:09 2014 gouy_e
** Last update Sun Apr 13 19:23:55 2014 gouy_e
*/

#include <stdlib.h>
#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "my.h"

int		check_isid(t_vmprop *vmprop, int nbr)
{
  t_champ	*tmp;

  tmp = vmprop->deb;
  while (tmp != NULL)
    {
      if (nbr == tmp->id)
        return (1);
      tmp = tmp->next;
    }
  return (0);
}

int		my_get_ctmo_arg(char *nbr,
				__attribute__((unused))int (*opts)[3],
				t_vmprop *vmprop)
{
  if ((vmprop->ctmo = my_getnbr(nbr)) <= 0)
    {
      vmprop->ctmo = 0;
      my_puterror("Bad value with '-ctmo': ");
      my_puterror(nbr);
      my_puterror(" - Option ignored\n");
    }
  return (0);
}
