/*
** my_fight.c for Corewar in /home/hirt_r/corewar
** 
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
** 
** Started on  Mon Mar 31 23:16:10 2014 Romain Hirt
** Last update Sat Apr 26 18:45:40 2014 gouy_e
*/

#include <stdlib.h>
#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "convert_arg.h"
#include "my.h"
#include "file_create.h"

void		my_set_cycle(t_vmprop *vmprop)
{
  t_champ	*tmp;

  tmp = vmprop->deb;
  while (tmp != NULL)
    {
      tmp->cycle = (vmprop->tabcycle)[(int)(tmp->body)[0]];
      tmp = tmp->next;
    }
}

int		execute_champ(t_champ *champ, size_v vm[], t_vmprop *vmprop)
{
  int		cmd;

  cmd = (int)vm[champ->pc % MEM_SIZE];
  if (cmd >= 1 && cmd <= 17)
    {
      if (vmprop->ptr[cmd](vm, champ, vmprop) != 0)
	return (-1);
      if (vm[(champ->pc % MEM_SIZE)] >= 1 && vm[(champ->pc % MEM_SIZE)] <= 16)
	champ->cycle = (vmprop->tabcycle)[(int)vm[(int)(champ->pc % MEM_SIZE)]];
      else
	champ->cycle = 1;
    }
  else
    {
      ++(champ->pc);
      champ->cycle = 1;
    }
  return (0);
}

int		execute_cycle(size_v vm[], t_vmprop *vmprop)
{
  t_champ	*tmp;
  int		i;

  tmp = vmprop->deb;
  while (tmp != NULL && vmprop->champ_tot > 1)
    {
      if (tmp->base == 0 && tmp->live == vmprop->cycle_to_die)
	{
	  my_write(tmp, vmprop, 3, 0);
	  my_printf("%s (%d) est mort\n", tmp->head.prog_name, tmp->id);
	  i = tmp->id;
	  tmp = tmp->next;
	  my_die(i, vmprop);
	  --(vmprop->champ_tot);
	}
      else if (--(tmp->cycle) <= 0)
	if (execute_champ(tmp, vm, vmprop) != 0)
	  return (-1);
      ++(tmp->live);
      tmp = tmp->next;
    }
  return (0);
}

void		my_litle_live(t_vmprop *vmprop)
{
  t_champ	*tmp;
  int		temp;

  vmprop->last = vmprop->deb;
  tmp = vmprop->deb;
  temp = tmp->live;
  while (tmp != NULL)
    {
      if (tmp->base == 0 && tmp->live > temp)
	{
	  temp = tmp->live;
	  vmprop->last = tmp;
	}
      tmp = tmp->next;
    }
}

int		my_start_fight(size_v vm[], t_vmprop *vmprop)
{
  vmprop->cycle_to_die = CYCLE_TO_DIE;
  vmprop->nb_live = 0;
  vmprop->nb_cycle = 0;
  init_cycle(vmprop->tabcycle);
  init_func(vmprop);
  my_set_cycle(vmprop);
  while (vmprop->cycle_to_die > 100 &&
	 vmprop->dump != 0 && vmprop->champ_tot > 1)
    {
      ++(vmprop->nb_cycle);
      if (vmprop->dump >= 0)
	--(vmprop->dump);
      if (execute_cycle(vm, vmprop) == -1)
	{
	  vmprop->last = vmprop->deb;
	  vmprop->dump = 0;
	  return (-1);
	}
    }
  my_litle_live(vmprop);
  return (0);
}
