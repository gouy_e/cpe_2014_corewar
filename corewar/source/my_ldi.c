/*
** my_ldi.c for corewar in /home/gouy_e/rendu/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Mon Apr  7 23:47:21 2014 gouy_e
** Last update Thu Apr 10 16:57:18 2014 gouy_e
*/

#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "read_oct.h"
#include "my.h"

int		my_ldi(size_v vm[], t_champ *champ)
{
  int		s;
  int		tmp;
  int		value;

  tmp = champ->pc;
  get_func_arg(champ, vm);
  s = (champ->val)[0] + (champ->val)[1];
  value = my_dir(s, vm, tmp);
  if ((champ->arg)[2] >= 1 && (champ->arg)[2] <= REG_NUMBER)
    {
      if (((champ->reg)[(champ->arg)[2]] = value))
	champ->carry = 0;
      else
	champ->carry = 1;
    }
  return (0);
}
