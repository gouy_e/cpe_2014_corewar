/*
** my_live.c for corewar in /home/gouy_e/rendu/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Mon Apr  7 20:38:55 2014 gouy_e
** Last update Fri Apr 11 19:52:40 2014 gouy_e
*/

#include <stdlib.h>
#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "read_oct.h"
#include "my.h"

int		my_live(size_v vm[], t_champ *champ, t_vmprop *vmprop)
{
  t_champ	*tmp;
  int		value;

  ++(vmprop->nb_live);
  if (vmprop->nb_live == NBR_LIVE)
    {
      vmprop->nb_live = 0;
      vmprop->cycle_to_die -= CYCLE_DELTA;
    }
  tmp = vmprop->deb;
  value = my_addr_dir(champ, vm);
  champ->pc = (champ->pc + 1) % MEM_SIZE;
  while (tmp != NULL && tmp->base == 0)
    {
      if (value == tmp->id)
	{
	  if (vmprop->silent == 0)
	    my_printf("%s (%d) est en vie\n", tmp->head.prog_name, tmp->id);
	  tmp->live = 0;
	  vmprop->last = tmp;
	  return (0);
	}
      tmp = tmp->next;
    }
  return (0);
}
