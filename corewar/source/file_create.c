/*
** file_create.c for corewar in /home/gouy_e/rendu/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Wed Apr  9 14:15:17 2014 gouy_e
** Last update Fri Apr 11 19:34:37 2014 gouy_e
*/

#include <stdlib.h>
#include <unistd.h>
#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "struct_comb.h"
#include "my.h"

int		my_init_write(t_vmprop *vmprop)
{
  t_champ	*tmp;
  char		temp;
  int		i;
  int		j;

  if (vmprop->fd == -1)
    return (0);
  temp = (char)vmprop->champ_tot;
  write(vmprop->fd, &temp, sizeof(temp));
  tmp = vmprop->deb;
  i = -1;
  while (++i != MEM_SIZE)
    (vmprop->vm_id)[i] = 0;
  while (tmp != NULL)
    {
      write(vmprop->fd, &(tmp->head), sizeof(header_t));
      i = tmp->pc;
      j = -1;
      while (++j != tmp->head.prog_size)
	(vmprop->vm_id)[i++] = tmp->id_x;
      tmp = tmp->next;
    }
  write(vmprop->fd, vmprop->vm_id, MEM_SIZE);
  return (0);
}

void		my_init_struct(char *tmp)
{
  int		i;

  i = -1;
  while (++i != sizeof(t_comb))
    tmp[i] = 0;
}

int		my_write(t_champ *champ, t_vmprop *vmprop, int param, int pos)
{
  t_comb	tmp;
  int		temp;

  my_init_struct((char *)&tmp);
  temp = (pos % MEM_SIZE + MEM_SIZE) % MEM_SIZE;
  if (vmprop->fd == -1 || (vmprop->vm_id)[temp] == champ->id_x)
    return (0);
  tmp.opt = (char)param;
  tmp.id = champ->id_x;
  tmp.pos = (short int)temp;
  tmp.nb_cycle = vmprop->nb_cycle;
  write(vmprop->fd, &tmp, sizeof(tmp));
  (vmprop->vm_id)[temp] = champ->id_x;
  return (0);
}
