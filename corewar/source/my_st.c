/*
** my_st.c for corewar in /home/gouy_e/rendu/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Mon Apr  7 21:38:41 2014 gouy_e
** Last update Sun Apr 13 19:14:25 2014 gouy_e
*/

#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "read_oct.h"
#include "my.h"
#include "file_create.h"

int		my_st(size_v vm[], t_champ *champ, t_vmprop *vmprop)
{
  int		tmp;
  int		temp;

  tmp = champ->pc;
  get_func_arg(champ, vm);
  temp = ((tmp + (champ->arg)[1]) % MEM_SIZE + MEM_SIZE) % MEM_SIZE;
  if ((champ->type)[1] == 1 &&
      (champ->arg)[1] >= 1 &&
      (champ->arg)[1] <= REG_NUMBER)
    (champ->reg)[(champ->arg)[1]] = (champ->val)[0];
  else if ((champ->type)[1] == 3 &&
	   (vmprop->nb_cycle > vmprop->ctmo ||
	    (temp >= champ->min && temp <= champ->max)))
    {
      my_write(champ, vmprop, 0, tmp + (champ->arg)[1]);
      my_cp(vm, tmp + (champ->arg)[1], champ->val[0]);
    }
  return (0);
}
