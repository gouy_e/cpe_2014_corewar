/*
** my_getfunc_args_2.c for corewar in /home/gouy_e/rendu/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Thu Apr 10 17:18:33 2014 gouy_e
** Last update Fri Apr 11 17:48:27 2014 gouy_e
*/

#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "my.h"

int		my_reg(t_champ *champ, int i)
{
  if ((champ->arg)[i] >= 1 && (champ->arg)[i] <= REG_NUMBER)
    return ((champ->reg)[(champ->arg)[i]]);
  return (0);
}

int		my_addr_ind(t_champ *champ, size_v vm[], int cmd)
{
  short int	ret;
  int		i;

  i = -1;
  ret = 0;
  while (++i != 2)
    ret = (ret << 8) + vm[(++champ->pc) % MEM_SIZE];
  i = (int)ret;
  if (cmd <= 12 || cmd == 16)
    i = i % IDX_MOD;
  return (i);
}

int		my_addr_dir(t_champ *champ, size_v vm[])
{
  int		ret;
  int		i;

  i = -1;
  ret = 0;
  while (++i != 4)
    ret = (ret << 8) + vm[(++champ->pc) % MEM_SIZE];
  return (ret);
}

int		my_get_addr_arg(t_champ *champ, size_v temp,
				size_v vm[], int val[4])
{
  if (temp == 1)
    return (vm[(++champ->pc) % MEM_SIZE]);
  else if (temp == 3 || val[0] == 1)
    return (my_addr_ind(champ, vm, val[1]));
  else if (temp == 2)
    return (my_addr_dir(champ, vm));
  return (0);
}
