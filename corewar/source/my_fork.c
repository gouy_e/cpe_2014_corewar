/*
** my_fork.c for corewar in /home/gouy_e/rendu/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Apr  8 00:25:01 2014 gouy_e
** Last update Sun Apr 13 21:12:27 2014 gouy_e
*/

#include <stdlib.h>
#include <string.h>
#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "read_oct.h"
#include "my.h"
#include "my_list_needed.h"

void		my_cp_reg(t_champ *champ, t_champ *tmp)
{
  int		i;

  i = -1;
  tmp->id_x = champ->id_x;
  while (++i != REG_NUMBER + 1)
    (tmp->reg)[i] = (champ->reg)[i];
}

void		get_new_champ(t_champ *champ, size_v vm[], t_vmprop *vmprop)
{
  t_champ	*tmp;

  tmp = champ;
  while (tmp->next != NULL)
    tmp = tmp->next;
  tmp->carry = champ->carry;
  if (vm[tmp->pc % MEM_SIZE] >= 1 && vm[tmp->pc % MEM_SIZE] <= 16)
    tmp->cycle = (vmprop->tabcycle)[vm[tmp->pc % MEM_SIZE]];
  else
    tmp->cycle = 1;
  my_cp_reg(champ, tmp);
}

int             my_fork(size_v vm[], t_champ *champ, t_vmprop *vmprop)
{
  int		opt[3];
  int		size[2];

  opt[0] = champ->id;
  opt[1] = champ->pc;
  opt[1] += my_addr_ind(champ, vm, vm[champ->pc % MEM_SIZE]);
  opt[1] = (opt[1] % MEM_SIZE + MEM_SIZE) % MEM_SIZE;
  opt[2] = 1;
  size[0] = champ->min;
  size[1] = champ->max;
  champ->pc = (champ->pc + 1) % MEM_SIZE;
  if (my_put_in_end_list(&(vmprop->deb), opt, size) == 1)
    {
      my_puterror("can't perform malloc\n");
      return (1);
    }
  get_new_champ(champ, vm, vmprop);
  return (0);
}
