/*
** my_get_silent.c for corewar in /home/gouy_e/rendu/CPE_2014_corewar/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Fri Apr 11 20:08:24 2014 gouy_e
** Last update Fri Apr 11 20:09:19 2014 gouy_e
*/

#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "my.h"

int		my_get_silent_arg(char *name,
				  __attribute__((unused))int (*opts)[4],
				  t_vmprop *vmprop)
{
  if (my_strcmp(name, "yes") == 0 || my_strcmp("oui", name) == 0)
    vmprop->silent = 1;
  else
    vmprop->silent = 0;
  return (0);
}
