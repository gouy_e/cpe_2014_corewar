/*
** my_get_args.c for corewar in /home/gouy_e/corewar/sources
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Sun Mar 30 22:40:38 2014 gouy_e
** Last update Sun Apr 13 15:41:28 2014 gouy_e
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "my.h"
#include "opts.h"

int		reverse_endian(int a)
{
  int		x;
  char		*y;

  x = 1;
  y = (char *) & x;
  if (*y == 1)
    {
      x = 0;
      x += a & 0xff;
      x = x << 8;
      x += (a >> 8) & 0xff;
      x = x << 8;
      x += (a >> 16) & 0xff;
      x = x << 8;
      x += (a >> 24) & 0xff;
      return (x);
    }
  return (a);
}

int		my_get_n_arg(char *nbr,
                             int (*opts)[3], t_vmprop *vmprop)
{
  if (((*opts)[0] = my_getnbr(nbr)) <= 0 ||
      check_isid(vmprop, (*opts)[0]) == 1)
    {
      (*opts)[0] = -1;
      my_puterror("Bad value with '-n': ");
      my_puterror(nbr);
      my_puterror(" - Option ignored\n");
    }
  return (0);
}

int		my_get_a_arg(char *nbr,
			     int (*opts)[3])
{
  if (((*opts)[1] = my_getnbr(nbr)) == 0
      && nbr[0] != 48 && nbr[1] != 0)
    {
      (*opts)[1] = -1;
      my_puterror("Bad value with '-a' : ");
      my_puterror(nbr);
      my_puterror(" - Option ignored\n");
    }
  else
    (*opts)[1] = ((*opts)[1] % MEM_SIZE + MEM_SIZE) % MEM_SIZE;
  return (0);
}

int		my_get_dump_arg(char *nbr,
				__attribute__((unused))int (*opts)[4],
				t_vmprop *vmprop)
{
  if ((vmprop->dump = my_getnbr(nbr)) == 0
      && nbr[0] != 48 && nbr[1] != 0)
    {
      vmprop->dump = -1;
      my_puterror("Bad value with '-dump' : ");
      my_puterror(nbr);
      my_puterror(" - Option ignored\n");
    }
  else
    vmprop->dump = (vmprop->dump % MEM_SIZE + MEM_SIZE) % MEM_SIZE;
  return (0);
}

int		my_get_x_arg(char *name,
			     __attribute__((unused))int (*opts)[4],
			     t_vmprop *vmprop)
{
  if (vmprop->fd != -1)
    close(vmprop->fd);
  if ((vmprop->fd = open(name, O_CREAT | O_WRONLY | O_TRUNC, 0644)) == -1)
    {
      my_puterror("Bad value with '-X' : ");
      my_puterror(name);
      my_puterror(" - Option ignored\n");
    }
  return (0);
}
