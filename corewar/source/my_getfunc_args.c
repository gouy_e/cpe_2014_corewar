/*
** my_getfunc_args.c for my_getfunc_args.c in /home/gouy_e/rendu/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Mon Apr  7 16:33:54 2014 gouy_e
** Last update Thu Apr 10 17:53:07 2014 gouy_e
*/

#include "op.h"
#include "champ.h"
#include "vmprop.h"
#include "my.h"
#include "read_oct.h"

int		my_ind(int nb, size_v vm[], int pc)
{
  short int	ret;
  int		j;

  j = -1;
  ret = 0;
  nb = (nb % MEM_SIZE + MEM_SIZE) % MEM_SIZE;
  while (++j != 2)
    ret = (ret << 8) + vm[(nb + j + pc) % MEM_SIZE];
  return ((int)ret);
}

int		my_dir(int nb, size_v vm[], int pc)
{
  int		ret;
  int		j;

  j = -1;
  ret = 0;
  nb = (nb % MEM_SIZE + MEM_SIZE) % MEM_SIZE;
  while (++j != 4)
    ret = (ret << 8) + vm[(nb + j + pc) % MEM_SIZE];
  return (ret);
}

int		isind(int cmd, int i)
{
  if ((i == 0 || i == 1) && (cmd == 10 || cmd == 15))
    return (1);
  else if ((i == 1 || i == 2) && cmd == 11)
    return (1);
  return (0);
}

void		my_get_champval(t_champ *champ, size_v vm[],
				size_v n, int val[4])
{
  int		temp;

  val[0] = isind(val[1], val[2]);
  temp = (int)(n >> (6 - (val[2] * 2))) & 0x3;
  (champ->type)[val[2]] = (int)temp;
  (champ->arg)[val[2]] = my_get_addr_arg(champ, temp, vm, val);
  if (temp == 1)
    (champ->val)[val[2]] = my_reg(champ, val[2]);
  else if (temp == 2)
    (champ->val)[val[2]] = (champ->arg)[val[2]];
  else if (temp == 3 && val[0] == 1)
    (champ->val)[val[2]] = my_ind((champ->arg)[val[2]], vm, val[3]);
  else if (temp == 3 && val[0] == 0)
    (champ->val)[val[2]] = my_dir((champ->arg)[val[2]], vm, val[3]);
  else
    (champ->val)[val[2]] = 0;
}

void		get_func_arg(t_champ *champ, size_v vm[])
{
  size_v	n;
  int		val[4];

  val[1] = (int)vm[champ->pc];
  val[3] = champ->pc;
  n = vm[(++champ->pc) % MEM_SIZE];
  val[2] = -1;
  while (++(val[2]) != 3)
    my_get_champval(champ, vm, n, val);
  champ->pc = (champ->pc + 1) % MEM_SIZE;
}
