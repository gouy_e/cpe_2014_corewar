/*
** struct_champ.h for corewar in /home/gouy_e/corewar/headers
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Sun Mar 30 22:24:53 2014 gouy_e
** Last update Fri Apr 11 17:46:51 2014 gouy_e
*/

#ifndef FIGHT_H_
# define FIGHT_H_

int	my_start_fight(size_v [], t_vmprop *);

#endif /* !FIGHT_H_ */
