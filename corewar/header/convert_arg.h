/*
** convert_arg.h for convert_arg.h in /home/gouy_e/rendu/corewar/header
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Mon Apr  7 19:00:16 2014 gouy_e
** Last update Tue Apr  8 17:35:02 2014 gouy_e
*/

#ifndef CONVERT_ARG_H_
# define CONVERT_ARG_H_

void		get_func_arg(t_champ *, size_v []);
void		my_die(int, t_vmprop *);

#endif /* !CONVERT_ARG_H_ */
