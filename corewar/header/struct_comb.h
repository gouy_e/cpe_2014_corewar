/*
** struct_comb.h for corewar in /home/gouy_e/rendu/corewar/header
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Wed Apr  9 14:27:36 2014 gouy_e
** Last update Sun Apr 13 14:27:36 2014 gouy_e
*/

#ifndef STRUCT_COMB_H_
# define STRUCT_COMB_H_

struct			s_comb
{
  char			opt;
  short int		pos;
  char			id;
  int			nb_cycle;
};

typedef struct s_comb	t_comb;

#endif /* !STRUCT_COMB_H_ */
