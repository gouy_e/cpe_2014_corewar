/*
** struct_arg.h for corewar in /home/gouy_e/corewar/headers
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Sun Mar 30 22:35:18 2014 gouy_e
** Last update Sun Apr 13 14:27:06 2014 gouy_e
*/

#ifndef STRUCT_ARG_H_
# define STRUCT_ARG_H_

struct			s_arg
{
  char			*pattern;
  int			(*fptr)(char *, int (*)[], t_vmprop *);
};

typedef struct s_arg	t_arg;

#endif /* !STRUCT_ARG_H_ */
