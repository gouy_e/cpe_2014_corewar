/*
** struct_champ.h for corewar in /home/gouy_e/corewar/headers
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Sun Mar 30 22:24:53 2014 gouy_e
** Last update Sun Apr 13 18:47:15 2014 gouy_e
*/

#ifndef VMPROP_H_
# define VMPROP_H_

# define size_v		unsigned char

struct			s_vmprop
{
  int			dump;
  t_champ		*deb;
  t_champ		*last;
  int			cycle_to_die;
  int			nb_live;
  int			tabcycle[18];
  int			(*ptr[18])(size_v [], t_champ *, struct s_vmprop *);
  int			nb_cycle;
  int			champ_tot;
  int			fd;
  char			vm_id[MEM_SIZE];
  int			silent;
  int			ctmo;
};

typedef struct s_vmprop	t_vmprop;

void			init_cycle(int []);
void			init_func(t_vmprop []);

#endif /* !VMPROP_H_ */
