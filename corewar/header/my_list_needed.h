/*
** my_list_needed.h for corewar in /home/gouy_e/rendu/corewar/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Mon Apr  7 15:23:08 2014 gouy_e
** Last update Sun Apr 13 19:07:01 2014 gouy_e
*/

#ifndef MY_LIST_NEEDED_H_
# define MY_LIST_NEEDED_H_

int	my_put_in_end_list(t_champ **, int [], int []);

#endif /* !MY_LIST_NEEDED_H_ */
