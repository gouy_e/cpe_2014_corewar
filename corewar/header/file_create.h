/*
** file_create.h for corewar in /home/gouy_e/rendu/corewar/header
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Wed Apr  9 14:17:06 2014 gouy_e
** Last update Wed Apr  9 19:37:25 2014 gouy_e
*/

#ifndef FILE_CREATE_H_
# define FILE_CREATE_H_

int		my_write(t_champ *, t_vmprop *, int, int);
int		my_init_write(t_vmprop *, size_v []);

#endif /* !FILE_CREATE_H_ */
