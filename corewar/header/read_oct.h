/*
** read_oct.h for corewar in /home/gouy_e/rendu/corewar/header
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Mon Apr  7 20:48:31 2014 gouy_e
** Last update Fri Apr 11 17:39:12 2014 gouy_e
*/

#ifndef READ_OCT_H_
# define READ_OCT_H_

int	my_addr_dir(t_champ *, size_v []);
void	get_func_arg(t_champ *, size_v []);
void	my_cp(size_v [], int, int);
int	my_addr_ind(t_champ *, size_v [], int);
int	my_dir(int, size_v [], int);
int	my_reg(t_champ *, int);
int	my_get_addr_arg(t_champ *, size_v, size_v [], int []);

#endif /* !READ_OCT_H_ */
