/*
** get_arg.h for corewar in /home/gouy_e/corewar/headers
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Sun Mar 30 22:42:15 2014 gouy_e
** Last update Sun Apr 13 19:19:50 2014 gouy_e
*/

#ifndef GET_ARG_H_
# define GET_ARG_H_

int	my_get_champ_arg(char *, t_champ **, int []);
int	my_get_dump_arg(char *, int (*)[], t_vmprop *);
int	my_get_n_arg(char *, int (*)[], t_vmprop *);
int	my_get_silent_arg(char *, int (*)[], t_vmprop *);
int	my_get_a_arg(char *, int (*)[], t_vmprop *);
int	my_get_x_arg(char *, int (*)[], t_vmprop *);
int	my_get_ctmo_arg(char *, int (*)[], t_vmprop *);
int	my_set_option(char **, t_vmprop *, t_arg *);
int	reverse_endian(int);

#endif /* !GET_ARG_H_ */
