/*
** opts.h for corewar in /home/gouy_e/rendu/CPE_2014_corewar/corewar/header
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Sun Apr 13 15:40:32 2014 gouy_e
** Last update Sun Apr 13 15:41:10 2014 gouy_e
*/

#ifndef OPTS_H_
# define OPTS_H_

int	check_isid(t_vmprop *, int);

#endif /* !OPTS_H_ */
