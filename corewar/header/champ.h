/*
** struct_champ.h for corewar in /home/gouy_e/corewar/headers
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Sun Mar 30 22:24:53 2014 gouy_e
** Last update Sun Apr 13 19:10:24 2014 gouy_e
*/

#ifndef CHAMP_H_
# define CHAMP_H_

struct			s_champ
{
  header_t		head;
  char			*body;
  int			reg[REG_NUMBER + 1];
  int			id;
  int			base;
  int			pc;
  char			carry;
  int			cycle;
  int			type[MAX_ARGS_NUMBER];
  int			arg[MAX_ARGS_NUMBER];
  int			val[MAX_ARGS_NUMBER];
  int			live;
  char			id_x;
  int			min;
  int			max;
  struct s_champ	*next;
};

typedef struct s_champ	t_champ;

#endif /* !CHAMP_H_ */
