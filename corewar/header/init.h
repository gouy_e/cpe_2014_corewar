/*
** init.h for corewar in /home/gouy_e/rendu/corewar/header
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Thu Apr 10 18:31:37 2014 gouy_e
** Last update Sun Apr 13 22:13:32 2014 gouy_e
*/

#ifndef INIT_H_
# define INIT_H_

int	my_init_tabarg(t_arg **);
void	my_init_reg(t_champ *);
void	my_clean_vm(size_v []);
int	my_init_vm(t_vmprop *, size_v []);

#endif /* !INIT_H_ */
