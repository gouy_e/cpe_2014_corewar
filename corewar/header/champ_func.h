/*
** champ_func.h for corewar in /home/gouy_e/rendu/corewar/header
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Mon Apr  7 20:50:08 2014 gouy_e
** Last update Sun Apr 13 18:46:49 2014 gouy_e
*/

#ifndef CHAMP_FUNC_H_
# define CHAMP_FUNC_H_

int	my_live(size_v [], t_champ *, t_vmprop *);
int	my_ld(size_v [], t_champ *, t_vmprop *);
int	my_st(size_v [], t_champ *, t_vmprop *);
int	my_add(size_v [], t_champ *, t_vmprop *);
int	my_and(size_v [], t_champ *, t_vmprop *);
int	my_or(size_v [], t_champ *, t_vmprop *);
int	my_xor(size_v [], t_champ *, t_vmprop *);
int	my_sub(size_v [], t_champ *, t_vmprop *);
int	my_zjmp(size_v [], t_champ *, t_vmprop *);
int	my_sti(size_v [], t_champ *, t_vmprop *);
int	my_ldi(size_v [], t_champ *, t_vmprop *);
int	my_fork(size_v [], t_champ *, t_vmprop *);
int	my_aff(size_v [], t_champ *, t_vmprop *);
int	my_gtmd(size_v [], t_champ *, t_vmprop *);

#endif /* !CHAMP_FUNC_H_ */
